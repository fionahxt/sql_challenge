# User input for desired start and end time, same day only
set @start_time = "2010-10-11 9:00", @end_time = "2010-10-11 14:00";

select format(sum(close_price*vol)/sum(vol),8) as VWAP, date_format(@start_time, '%d/%m/%Y') as 'Date', date_format(@start_time, '%H:%i') as 'Start Time', date_format(@end_time, '%H:%i') as 'End Time'
from sql_challenge_1.stock
where dt between @start_time and @end_time;