use sql_challenge_2;
drop temporary table if exists T1;
drop temporary table if exists T2;
drop temporary table if exists T3;
drop temporary table if exists T4;

create temporary table if not exists T1
select s1.Date, max(format(abs(s1.Open-s1.Close),4)) as 'Range'
from sql_challenge_2.stock s1
group by s1.Date;

create temporary table if not exists T2
select s2.Date, max(s2.High) as maxHigh
from sql_challenge_2.stock s2
group by s2.Date;

create temporary table if not exists T3
select s3.Date, s3.Time
from sql_challenge_2.stock s3, T2
where s3.Date = T2.Date and s3.High = T2.maxHigh;

create temporary table if not exists T4
select T1.Date, T1.Range, T3.Time
from T1 inner join T3 on T1.Date = T3.Date
order by T1.Range desc;

select T.Date, T4.Range, T4.Time
from T4,
	(select distinct T1.Date
	from T1 inner join T3 on T1.Date = T3.Date
	order by T1.Range desc
	limit 3) as T
where T4.Date = T.Date;